package com.abn.recipeservice.integration;

import com.abn.recipeservice.domain.Ingredient;
import com.abn.recipeservice.domain.Recipe;
import com.abn.recipeservice.domain.RecipeType;
import com.abn.recipeservice.dto.*;
import com.abn.recipeservice.repository.DomainType;
import com.abn.recipeservice.repository.IngredientRepository;
import com.abn.recipeservice.repository.QueryOperator;
import com.abn.recipeservice.repository.RecipeRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
public class RecipeIntegrationTest {
    private static RestTemplate restTemplate;
    @LocalServerPort
    private int port;
    private String baseUrl = "http://localhost";
    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private IngredientRepository ingredientRepository;


    @BeforeAll
    private static void init() {

        restTemplate = new RestTemplate();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        restTemplate.setRequestFactory(requestFactory);
    }

    @BeforeEach
    private void setUpAddress() {
        recipeRepository.deleteAll();
        ingredientRepository.deleteAll();
        baseUrl = baseUrl + ":" + port + "/recipe";
    }

    @Test
    void saveRecipeCorrectly() {

        Ingredient ingredient = ingredientRepository.save(Ingredient.builder().name("test").build());
        RecipeRequest recipeRequest = RecipeRequest.builder()
                .ingredients(Arrays.asList(ingredient.getId()))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();

        RecipeResponse recipeResponse = RecipeResponse.builder()
                .ingredients(Arrays.asList(IngredientResponse.builder()
                        .id(ingredient.getId())
                        .name(ingredient.getName())
                        .build()))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();

        ResponseEntity<RecipeResponse> actualResponseBody = restTemplate.exchange(baseUrl, HttpMethod.POST, new HttpEntity(recipeRequest), RecipeResponse.class);
        assertEquals(HttpStatus.OK, actualResponseBody.getStatusCode());
        RecipeResponse actualRecipeResponseBody = actualResponseBody.getBody();
        assertThat(actualRecipeResponseBody.getRecipeType()).isEqualTo(recipeResponse.getRecipeType());
        assertThat(actualRecipeResponseBody.getInstruction()).isEqualTo(recipeResponse.getInstruction());
        assertThat(actualRecipeResponseBody.getName()).isEqualTo(recipeResponse.getName());
        assertThat(actualRecipeResponseBody.getIngredients().size()).isEqualTo(recipeResponse.getIngredients().size());
        assertThat(actualRecipeResponseBody.getNumberServings()).isEqualTo(4);

    }

    @Test
    void updateRecipeCorrectly() {

        Ingredient ingredient = ingredientRepository.save(Ingredient.builder().name("test").build());
        Recipe recipe = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();
        recipe = recipeRepository.save(recipe);

        RecipeRequest recipeRequest = RecipeRequest.builder()
                .ingredients(Arrays.asList(ingredient.getId()))
                .recipeType(RecipeType.REGULAR)
                .instruction("test2")
                .name("test2")
                .numberServings(4)
                .build();

        RecipeResponse recipeResponse = RecipeResponse.builder()
                .ingredients(Arrays.asList(IngredientResponse.builder()
                        .id(ingredient.getId())
                        .name(ingredient.getName())
                        .build()))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();

        ResponseEntity<RecipeResponse> actualResponseBody = restTemplate.exchange(baseUrl + "/{id}", HttpMethod.PUT, new HttpEntity(recipeRequest), RecipeResponse.class, recipe.getId());
        assertEquals(HttpStatus.OK, actualResponseBody.getStatusCode());
        RecipeResponse actualRecipeResponseBody = actualResponseBody.getBody();
        assertThat(recipe.getRecipeType()).isEqualTo(recipeResponse.getRecipeType());
        assertThat(recipe.getInstruction()).isEqualTo(recipeResponse.getInstruction());
        assertThat(recipe.getName()).isEqualTo(recipeResponse.getName());
        assertThat(recipe.getIngredients().size()).isEqualTo(recipeResponse.getIngredients().size());

    }

    @Test
    void updateRecipeWhileRecipeIsNotAvailable() {
        RecipeRequest recipeRequest = RecipeRequest.builder()
                .ingredients(Arrays.asList(1L))
                .recipeType(RecipeType.REGULAR)
                .instruction("test2")
                .name("test2")
                .numberServings(4)
                .build();
        HttpClientErrorException.NotFound recipeNotFoundException = assertThrows(HttpClientErrorException.NotFound.class, () -> {
            restTemplate.exchange(baseUrl + "/{id}", HttpMethod.PUT, new HttpEntity(recipeRequest), RecipeResponse.class, 1);
        });
        assertEquals(HttpStatus.NOT_FOUND, recipeNotFoundException.getStatusCode());
    }

    @Test
    void getRecipeWhileRecipeIsNotAvailable() {
        RecipeRequest recipeRequest = RecipeRequest.builder()
                .ingredients(Arrays.asList(1L))
                .recipeType(RecipeType.REGULAR)
                .instruction("test2")
                .name("test2")
                .numberServings(4)
                .build();
        HttpClientErrorException.NotFound recipeNotFoundException = assertThrows(HttpClientErrorException.NotFound.class, () -> {
            restTemplate.exchange(baseUrl + "/{id}", HttpMethod.GET, new HttpEntity(null), RecipeResponse.class, 1);
        });
        assertEquals(HttpStatus.NOT_FOUND, recipeNotFoundException.getStatusCode());
    }

    @Test
    void getRecipeShouldWorkCorrectly() {
        Ingredient ingredient = ingredientRepository.save(Ingredient.builder().name("test").build());
        Recipe recipe = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();
        recipe = recipeRepository.save(recipe);

        RecipeResponse recipeResponse = RecipeResponse.builder()
                .ingredients(Arrays.asList(IngredientResponse.builder()
                        .id(ingredient.getId())
                        .name(ingredient.getName())
                        .build()))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();

        ResponseEntity<RecipeResponse> actualResponseBody = restTemplate.exchange(baseUrl + "/{id}", HttpMethod.GET, new HttpEntity(null), RecipeResponse.class, recipe.getId());
        assertEquals(HttpStatus.OK, actualResponseBody.getStatusCode());
        RecipeResponse actualRecipeResponseBody = actualResponseBody.getBody();
        assertThat(actualRecipeResponseBody.getRecipeType()).isEqualTo(recipeResponse.getRecipeType());
        assertThat(actualRecipeResponseBody.getInstruction()).isEqualTo(recipeResponse.getInstruction());
        assertThat(actualRecipeResponseBody.getName()).isEqualTo(recipeResponse.getName());
        assertThat(actualRecipeResponseBody.getIngredients().size()).isEqualTo(recipeResponse.getIngredients().size());
        assertThat(actualRecipeResponseBody.getNumberServings()).isEqualTo(4);

    }

    @Test
    void searchRecipeFourPersonAndPotatoIngredientShouldReturnCorrectThing() {
        Ingredient ingredient = ingredientRepository.save(Ingredient.builder().name("potatoes").build());
        Recipe first = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();
        Recipe secondRecipe = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .recipeType(RecipeType.REGULAR)
                .instruction("test2")
                .name("test2")
                .numberServings(1)
                .build();
        recipeRepository.saveAll(Arrays.asList(secondRecipe, first));

        CriteriaRequest numberServings = CriteriaRequest.builder()
                .domainType(DomainType.RECIPE)
                .key("numberServings")
                .operation(QueryOperator.EQUAL)
                .value("4").build();
        CriteriaRequest potatoes = CriteriaRequest.builder()
                .domainType(DomainType.INGREDIENT)
                .key("name")
                .operation(QueryOperator.EQUAL)
                .value("potatoes").build();

        SearchRequest searchRequest = SearchRequest.builder().searchCriteria(Arrays.asList(numberServings, potatoes)).build();

        ResponseEntity<List<RecipeResponse>> actualResponseBody = restTemplate.exchange(baseUrl + "/search", HttpMethod.POST, new HttpEntity(searchRequest), new ParameterizedTypeReference<List<RecipeResponse>>() {
        });
        assertEquals(HttpStatus.OK, actualResponseBody.getStatusCode());
        List<RecipeResponse> actualRecipeResponseBody = actualResponseBody.getBody();
        assertThat(actualRecipeResponseBody.size()).isEqualTo(1);
        assertThat(actualRecipeResponseBody.get(0).getName()).isEqualTo("test");
        assertThat(actualRecipeResponseBody.get(0).getIngredients().get(0).getName()).isEqualTo("potatoes");
    }

    @Test
    void searchRecipeWithoutSalmonIngredientAndOvenShouldReturnCorrectThing() {
        Ingredient ingredient = ingredientRepository.save(Ingredient.builder().name("potatoes").build());
        Recipe first = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .recipeType(RecipeType.REGULAR)
                .instruction("test oven")
                .name("test")
                .numberServings(4)
                .build();
        Recipe secondRecipe = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .recipeType(RecipeType.REGULAR)
                .instruction("test2")
                .name("test2")
                .numberServings(1)
                .build();
        recipeRepository.saveAll(Arrays.asList(secondRecipe, first));

        CriteriaRequest instruction = CriteriaRequest.builder()
                .domainType(DomainType.RECIPE)
                .key("instruction")
                .operation(QueryOperator.MATCH)
                .value("oven").build();
        CriteriaRequest salmon = CriteriaRequest.builder()
                .domainType(DomainType.INGREDIENT)
                .key("name")
                .operation(QueryOperator.NOT_EQUAL)
                .value("salmon").build();

        SearchRequest searchRequest = SearchRequest.builder().searchCriteria(Arrays.asList(instruction, salmon)).build();

        ResponseEntity<List<RecipeResponse>> actualResponseBody = restTemplate.exchange(baseUrl + "/search", HttpMethod.POST, new HttpEntity(searchRequest), new ParameterizedTypeReference<List<RecipeResponse>>() {
        });
        assertEquals(HttpStatus.OK, actualResponseBody.getStatusCode());
        List<RecipeResponse> actualRecipeResponseBody = actualResponseBody.getBody();
        assertThat(actualRecipeResponseBody.size()).isEqualTo(1);
        assertThat(actualRecipeResponseBody.get(0).getName()).isEqualTo("test");
        assertThat(actualRecipeResponseBody.get(0).getIngredients().get(0).getName()).isEqualTo("potatoes");
    }

    @Test
    void searchAllRecipesWithVeggieType() {
        Ingredient ingredient = ingredientRepository.save(Ingredient.builder().name("potatoes").build());
        Recipe first = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .recipeType(RecipeType.VEGGIE)
                .instruction("test oven")
                .name("test")
                .numberServings(4)
                .build();
        Recipe secondRecipe = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .recipeType(RecipeType.VEGGIE)
                .instruction("test2")
                .name("test2")
                .numberServings(1)
                .build();
        recipeRepository.saveAll(Arrays.asList(secondRecipe, first));

        CriteriaRequest numberServings = CriteriaRequest.builder()
                .domainType(DomainType.RECIPE)
                .key("recipeType")
                .operation(QueryOperator.EQUAL)
                .value("VEGGIE").build();


        SearchRequest searchRequest = SearchRequest.builder().searchCriteria(Arrays.asList(numberServings)).build();

        ResponseEntity<List<RecipeResponse>> actualResponseBody = restTemplate.exchange(baseUrl + "/search", HttpMethod.POST, new HttpEntity(searchRequest), new ParameterizedTypeReference<List<RecipeResponse>>() {
        });
        assertEquals(HttpStatus.OK, actualResponseBody.getStatusCode());
        List<RecipeResponse> actualRecipeResponseBody = actualResponseBody.getBody();
        assertThat(actualRecipeResponseBody.size()).isEqualTo(2);

    }
}
