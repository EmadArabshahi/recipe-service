package com.abn.recipeservice.service;

import com.abn.recipeservice.domain.Ingredient;
import com.abn.recipeservice.domain.Recipe;
import com.abn.recipeservice.domain.RecipeType;
import com.abn.recipeservice.dto.IngredientResponse;
import com.abn.recipeservice.dto.RecipeRequest;
import com.abn.recipeservice.dto.RecipeResponse;
import com.abn.recipeservice.exception.RecipeNotFoundException;
import com.abn.recipeservice.mapper.RecipeMapperImpl;
import com.abn.recipeservice.repository.IngredientRepository;
import com.abn.recipeservice.repository.RecipeRepository;
import com.abn.recipeservice.service.impl.RecipeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class RecipeServiceTest {
    @Mock
    private RecipeRepository recipeRepository;

    @Mock
    private IngredientRepository ingredientRepository;

    @Spy
    private RecipeMapperImpl recipeMapper;

    @InjectMocks
    private RecipeServiceImpl recipeService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void saveRecipeCorrectly() {

        Ingredient ingredient = Ingredient.builder().id(1l).name("test").build();
        Recipe recipe = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .id(2l)
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();


        Mockito.doReturn(recipe).when(recipeRepository).save(Mockito.any());
        Mockito.doReturn(Arrays.asList(ingredient)).when(ingredientRepository).findAllById(Mockito.any());


        RecipeRequest recipeRequest = RecipeRequest.builder()
                .ingredients(Arrays.asList(ingredient.getId()))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();

        RecipeResponse recipeResponse = RecipeResponse.builder()
                .ingredients(Arrays.asList(IngredientResponse.builder()
                        .id(ingredient.getId())
                        .name(ingredient.getName())
                        .build()))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();

        RecipeResponse actualRecipeResponseBody = recipeService.save(recipeRequest);

        assertThat(actualRecipeResponseBody.getRecipeType()).isEqualTo(recipeResponse.getRecipeType());
        assertThat(actualRecipeResponseBody.getInstruction()).isEqualTo(recipeResponse.getInstruction());
        assertThat(actualRecipeResponseBody.getName()).isEqualTo(recipeResponse.getName());
        assertThat(actualRecipeResponseBody.getIngredients().size()).isEqualTo(recipeResponse.getIngredients().size());
        assertThat(actualRecipeResponseBody.getNumberServings()).isEqualTo(4);
        Mockito.verify(ingredientRepository, Mockito.times(1)).findAllById(Mockito.any());
        Mockito.verify(recipeRepository, Mockito.times(1)).save((Recipe) Mockito.any());

    }

    @Test
    void getRecipeCorrectly() {

        Ingredient ingredient = Ingredient.builder().id(1l).name("test").build();
        Recipe recipe = Recipe.builder().ingredients(Arrays.asList(ingredient))
                .id(2l)
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();

        Mockito.doReturn(Optional.ofNullable(recipe)).when(recipeRepository).findById(recipe.getId());

        RecipeResponse recipeResponse = RecipeResponse.builder()
                .ingredients(Arrays.asList(IngredientResponse.builder()
                        .id(ingredient.getId())
                        .name(ingredient.getName())
                        .build()))
                .recipeType(RecipeType.REGULAR)
                .instruction("test")
                .name("test")
                .numberServings(4)
                .build();

        RecipeResponse actualRecipeResponseBody = recipeService.get(2l);

        assertThat(actualRecipeResponseBody.getRecipeType()).isEqualTo(recipeResponse.getRecipeType());
        assertThat(actualRecipeResponseBody.getInstruction()).isEqualTo(recipeResponse.getInstruction());
        assertThat(actualRecipeResponseBody.getName()).isEqualTo(recipeResponse.getName());
        assertThat(actualRecipeResponseBody.getIngredients().size()).isEqualTo(recipeResponse.getIngredients().size());
        assertThat(actualRecipeResponseBody.getNumberServings()).isEqualTo(4);
        Mockito.verify(recipeRepository, Mockito.times(1)).findById(Mockito.any());

    }

    @Test
    void getRecipeWhenIsNotAvailableShouldReturnException() {
        Mockito.doReturn(Optional.empty()).when(recipeRepository).findById(1l);
        assertThrows(RecipeNotFoundException.class, () -> {
            recipeService.get(2l);
        });
        Mockito.verify(recipeRepository, Mockito.times(1)).findById(Mockito.any());
    }


}
