insert into INGREDIENT (name, id)
values ('test', 1);
insert into INGREDIENT (name, id)
values ('potato', 2);
insert into INGREDIENT (name, id)
values ('salmon', 3);

insert into RECIPE (instruction, name, number_of_servings, recipe_type, id)
values ('test oven', 'TEST', 4, 'VEGGIE', 4);
insert into RECIPE_INGREDIENTS (recipe_id, ingredient_id)
values (4, 2);

insert into RECIPE (instruction, name, number_of_servings, recipe_type, id)
values ('test oven', 'TEST 2', 3, 'REGULAR', 5);
insert into RECIPE_INGREDIENTS (recipe_id, ingredient_id)
values (5, 2);