package com.abn.recipeservice.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel
public class SearchRequest {
    @ApiModelProperty(position = 1, required = true, notes = "list of criterias")
    private List<CriteriaRequest> searchCriteria;
}
