package com.abn.recipeservice.dto;

import com.abn.recipeservice.repository.DomainType;
import com.abn.recipeservice.repository.QueryOperator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ApiModel
public class CriteriaRequest {
    @ApiModelProperty(value = "name", notes = "you can use one of field names")
    private String key;
    @ApiModelProperty(notes = "value of field name")
    private String value;
    @ApiModelProperty(value = "EQUAL", notes = "an operator which you want")
    private QueryOperator operation;
    @ApiModelProperty(value = "RECIPE", notes = "which one of entities? RECIPE | INGREDIENT")
    private DomainType domainType;
}
