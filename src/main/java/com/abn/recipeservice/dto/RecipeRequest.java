package com.abn.recipeservice.dto;

import com.abn.recipeservice.domain.RecipeType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(value = "represents a recipe")
public class RecipeRequest {
    @ApiModelProperty(position = 1, required = true, value = "VEGGIE | REGULAR", notes = "VEGGIE | REGULAR")
    @NotNull
    private RecipeType recipeType;
    @ApiModelProperty(position = 2, required = true, value = "name of recipe", notes = "name of recipe")
    @NotEmpty(message = "please enter valid name")
    @NotNull
    private String name;
    @ApiModelProperty(position = 3, required = true, value = "instruction of food")
    @NotEmpty(message = "please enter valid instruction")
    @NotNull
    private String instruction;
    @ApiModelProperty(position = 4, required = true, value = "1", notes = "this instruction is for how many people")
    @NotNull
    @Min(value = 1, message = "The value must be 1 minimum")
    private Integer numberServings;
    @ApiModelProperty(position = 5, required = true, notes = "A food ingredient is a list of ingredients, each of which is a separate entity")
    @NotEmpty(message = "please add ingredients")
    @Size(min = 1, message = "please add 1 ingredient minimum")
    @NotNull
    private List<Long> ingredients;
}
