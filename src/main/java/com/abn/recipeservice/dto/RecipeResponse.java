package com.abn.recipeservice.dto;

import com.abn.recipeservice.domain.RecipeType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class RecipeResponse {
    private Long id;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private RecipeType recipeType;
    private String name;
    private String instruction;
    private Integer numberServings;
    private List<IngredientResponse> ingredients;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy hh:mm:ss")
    private Date createDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM-dd-yyyy hh:mm:ss")
    private Date updateDate;
}
