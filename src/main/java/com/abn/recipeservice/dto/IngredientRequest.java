package com.abn.recipeservice.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(value = "represents a ingredient of recipe")
public class IngredientRequest {
    @NotEmpty(message = "please enter valid name")
    @NotNull
    @ApiModelProperty(value = "ingredient", notes = "name of ingredient")
    private String name;
}
