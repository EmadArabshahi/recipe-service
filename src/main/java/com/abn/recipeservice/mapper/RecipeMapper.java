package com.abn.recipeservice.mapper;

import com.abn.recipeservice.domain.Ingredient;
import com.abn.recipeservice.domain.Recipe;
import com.abn.recipeservice.dto.RecipeRequest;
import com.abn.recipeservice.dto.RecipeResponse;
import com.abn.recipeservice.dto.SearchRequest;
import com.abn.recipeservice.repository.SearchCriteria;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface RecipeMapper {

    @Named(value = "ingredientIdsToIngredients")
    default List<Ingredient> ingredientIdsToIngredients(List<Long> ingredientIds) {
        return ingredientIds.stream().map(id -> Ingredient.builder().id(id).build()).collect(Collectors.toList());
    }

    @Mapping(target = "ingredients", source = "ingredients", qualifiedByName = "ingredientIdsToIngredients")
    Recipe recipeRequestToRecipe(RecipeRequest recipeRequest);

    @Mappings({
            @Mapping(target = "ingredients", source = "recipeRequest.ingredients", qualifiedByName = "ingredientIdsToIngredients")
            , @Mapping(target = "createDate", source = "recipe.createDate")
            , @Mapping(target = "name", source = "recipeRequest.name")
            , @Mapping(target = "id", source = "recipe.id")
            , @Mapping(target = "instruction", source = "recipeRequest.instruction")
            , @Mapping(target = "recipeType", source = "recipeRequest.recipeType")
            , @Mapping(target = "numberServings", source = "recipeRequest.numberServings")
    })
    Recipe recipeRequestAndRecipeToRecipe(RecipeRequest recipeRequest, Recipe recipe);

    RecipeResponse recipeToRecipeResponse(Recipe recipe);

    List<RecipeResponse> recipeListToRecipeResponseList(List<Recipe> recipes);

    default List<SearchCriteria> searchRequestToSearchCriteriaList(SearchRequest searchRequest) {

        return searchRequest.getSearchCriteria().stream().map(criteriaRequest -> SearchCriteria.builder()
                .domainType(criteriaRequest.getDomainType())
                .operation(criteriaRequest.getOperation())
                .value(criteriaRequest.getValue())
                .key(criteriaRequest.getKey()).build())
                .collect(Collectors.toList());
    }
}
