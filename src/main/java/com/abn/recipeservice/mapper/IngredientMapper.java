package com.abn.recipeservice.mapper;

import com.abn.recipeservice.domain.Ingredient;
import com.abn.recipeservice.dto.IngredientRequest;
import com.abn.recipeservice.dto.IngredientResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface IngredientMapper {

    Ingredient ingredientRequestToIngredient(IngredientRequest ingredientRequest);

    IngredientResponse ingredientToIngredientResponse(Ingredient ingredient);

    @Mappings({
            @Mapping(target = "name", source = "ingredientRequest.name")
            , @Mapping(target = "createDate", source = "ingredient.createDate")
            , @Mapping(target = "id", source = "ingredient.id")
    })
    Ingredient ingredientRequestAndIngredientToIngredient(IngredientRequest ingredientRequest, Ingredient ingredient);

    List<IngredientResponse> recipeListToRecipeResponseList(List<Ingredient> ingredients);
}
