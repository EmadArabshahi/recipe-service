package com.abn.recipeservice.domain;

public enum RecipeType {
    VEGGIE, REGULAR
}
