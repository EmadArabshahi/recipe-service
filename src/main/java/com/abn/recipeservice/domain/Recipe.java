package com.abn.recipeservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "recipe")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Enumerated(EnumType.STRING)
    private RecipeType recipeType;
    @Column(unique = true)
    private String name;
    private String instruction;
    @Column(name = "number_of_servings")
    private Integer numberServings;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "recipe_ingredients",
            joinColumns = {
                    @JoinColumn(name = "recipe_id", referencedColumnName = "id",
                            nullable = false, updatable = false)},
            inverseJoinColumns = {
                    @JoinColumn(name = "ingredient_id", referencedColumnName = "id",
                            nullable = false, updatable = false)})
    private List<Ingredient> ingredients;
    @Column(name = "create_date")
    @CreationTimestamp
    private Date createDate;
    @Column(name = "update_date")
    @UpdateTimestamp
    private Date updateDate;

}
