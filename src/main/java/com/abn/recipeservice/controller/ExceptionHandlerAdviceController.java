package com.abn.recipeservice.controller;

import com.abn.recipeservice.exception.base.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@ControllerAdvice
public class ExceptionHandlerAdviceController {

    private final String ERROR_CODE_SPLITTER_SIGN = "#";

    private final MessageSource messageSource;

    private final Logger logger = LoggerFactory.getLogger(ExceptionHandlerAdviceController.class.getName());

    @Autowired
    public ExceptionHandlerAdviceController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorContent handleBindException(Exception e) {
        return new ValidationErrorContent(getValidationErrorContents((BindException) e));
    }

    @ExceptionHandler(value = {Throwable.class, TypeMismatchException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public void translateException(HttpServletResponse response, InternalServerException exception) {
        logger.error("Internal Error Exception", exception);
        response.setStatus(HttpStatus.NOT_FOUND.value());
        renderJsonResponse(translateExceptionInternal(exception), response);
    }


    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorContent translateException(BusinessException exception) {
        return translateExceptionInternal(exception);
    }

    @ExceptionHandler(DataAccessException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public void translateException(HttpServletResponse response, DataAccessException exception) {
        renderJsonResponse(translateExceptionInternal(exception), response);
    }

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public void translateException(HttpServletResponse response, IllegalArgumentException exception) {
        renderJsonResponse(translateExceptionInternal(exception), response);
    }


    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorContent translateException(NotFoundException exception) {
        return translateExceptionInternal(exception);
    }


    private ErrorContent translateExceptionInternal(Exception exception) {
        ErrorContent errorContent = null;
        try {
            String message = messageSource.getMessage(exception.getClass().getName(), null, LocaleContextHolder.getLocale());
            String[] error = message.split(ERROR_CODE_SPLITTER_SIGN);
            errorContent = new ErrorContent(error[1], error[0]);
        } catch (Exception e) {
            errorContent = new ErrorContent("1000", exception.getMessage());
        }

        return errorContent;

    }

    private List<FieldErrorContent> getValidationErrorContents(BindException exception) {
        List<FieldErrorContent> errors = new ArrayList<>();
        for (ObjectError objectError : exception.getBindingResult().getAllErrors()) {
            FieldError fieldError = (FieldError) objectError;
            errors.add(new FieldErrorContent(fieldError.getCode(), fieldError.getDefaultMessage(), fieldError.getField()));
        }
        return errors;
    }

    private void renderJsonResponse(Object model, HttpServletResponse response) {
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        try {
            jsonConverter.write(model, MediaType.APPLICATION_JSON, new ServletServerHttpResponse(response));

        } catch (HttpMessageNotWritableException | IOException e) {
            logger.error("Http Message Not Writable Exception", e);
        }
    }

}
