package com.abn.recipeservice.controller;

import com.abn.recipeservice.dto.RecipeRequest;
import com.abn.recipeservice.dto.RecipeResponse;
import com.abn.recipeservice.dto.SearchRequest;
import com.abn.recipeservice.service.RecipeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/recipe")
@ApiOperation("Recipe API")
@RequiredArgsConstructor
public class RecipeController {

    private final RecipeService recipeService;

    @ApiOperation(value = "saveRecipe", notes = "Returns a recipe with ingredient list")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 400, message = "BadRequest"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = RecipeResponse.class)})
    @PostMapping
    public RecipeResponse saveRecipe(@RequestBody @Valid RecipeRequest recipeRequest) {
        return recipeService.save(recipeRequest);
    }

    @ApiOperation(value = "getRecipe", notes = "Returns a recipe by id")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 400, message = "BadRequest"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = RecipeResponse.class)})
    @GetMapping("/{recipeId}")
    public RecipeResponse get(@PathVariable Long recipeId) {
        return recipeService.get(recipeId);
    }

    @ApiOperation(value = "getRecipe", notes = "delete a recipe by id")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 200, message = "Successful retrieval")})
    @DeleteMapping("/{recipeId}")
    public void delete(@PathVariable Long recipeId) {
        recipeService.delete(recipeId);
    }

    @ApiOperation(value = "updateRecipe", notes = "update a recipe by id and request body")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = RecipeResponse.class)})
    @PutMapping("/{recipeId}")
    public RecipeResponse update(@RequestBody @Valid RecipeRequest recipeRequest, @PathVariable Long recipeId) {
        return recipeService.update(recipeRequest, recipeId);
    }

    @ApiOperation(value = "searchRecipe", notes = "search a all recipes")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 400, message = "BadRequest"),
            @ApiResponse(code = 200, message = "Successful retrieval")})
    @PostMapping("/search")
    public List<RecipeResponse> search(@RequestBody SearchRequest searchCriteria) {
        return recipeService.search(searchCriteria);
    }
}
