package com.abn.recipeservice.controller;

import com.abn.recipeservice.dto.IngredientRequest;
import com.abn.recipeservice.dto.IngredientResponse;
import com.abn.recipeservice.service.IngredientService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ingredient")
@ApiOperation("Ingredient API")
@RequiredArgsConstructor
public class IngredientController {

    private final IngredientService ingredientService;

    @ApiOperation(value = "saveRecipe", notes = "Save and returns a ingredient")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 400, message = "BadRequest"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = IngredientResponse.class)})
    @PostMapping
    public IngredientResponse save(@RequestBody @Valid IngredientRequest ingredientRequest) {
        return ingredientService.save(ingredientRequest);
    }

    @ApiOperation(value = "getRecipe", notes = "Returns a ingredient by id")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 400, message = "BadRequest"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = IngredientResponse.class)})
    @GetMapping("/{ingredientId}")
    public IngredientResponse get(@PathVariable Long ingredientId) {
        return ingredientService.get(ingredientId);
    }

    @ApiOperation(value = "getRecipe", notes = "delete a ingredient by id")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 200, message = "Successful retrieval")})
    @DeleteMapping
    public void delete(Long ingredientId) {
        ingredientService.delete(ingredientId);
    }

    @ApiOperation(value = "getRecipe", notes = "Update a ingredient by id and request body")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 400, message = "BadRequest"),
            @ApiResponse(code = 200, message = "Successful retrieval",
                    response = IngredientResponse.class)})
    @PutMapping("/{ingredientId}")
    public IngredientResponse update(@RequestBody @Valid IngredientRequest ingredientRequest, @PathVariable Long ingredientId) {
        return ingredientService.update(ingredientRequest, ingredientId);
    }

    @ApiOperation(value = "getRecipe", notes = "Return list of ingredients")
    @ApiResponses(value = {
            @ApiResponse(code = 500, message = "Server error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 400, message = "BadRequest"),
            @ApiResponse(code = 200, message = "Successful retrieval")})
    @GetMapping("/{pageSize}/{page}")
    public List<IngredientResponse> getAll(@PathVariable Integer page, @PathVariable Integer pageSize) {
        return ingredientService.getAll(page, pageSize);
    }
}
