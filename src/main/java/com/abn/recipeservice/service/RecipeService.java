package com.abn.recipeservice.service;

import com.abn.recipeservice.dto.RecipeRequest;
import com.abn.recipeservice.dto.RecipeResponse;
import com.abn.recipeservice.dto.SearchRequest;

import java.util.List;


public interface RecipeService {

    RecipeResponse save(RecipeRequest recipeRequest);

    RecipeResponse get(Long recipeId);

    void delete(Long recipeId);

    RecipeResponse update(RecipeRequest recipeRequest, Long recipeId);

    List<RecipeResponse> search(SearchRequest searchCriteria);

}
