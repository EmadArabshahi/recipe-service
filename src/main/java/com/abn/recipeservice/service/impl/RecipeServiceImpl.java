package com.abn.recipeservice.service.impl;

import com.abn.recipeservice.domain.Ingredient;
import com.abn.recipeservice.domain.Recipe;
import com.abn.recipeservice.dto.RecipeRequest;
import com.abn.recipeservice.dto.RecipeResponse;
import com.abn.recipeservice.dto.SearchRequest;
import com.abn.recipeservice.exception.IngredientNotFoundException;
import com.abn.recipeservice.exception.RecipeNotFoundException;
import com.abn.recipeservice.mapper.RecipeMapper;
import com.abn.recipeservice.repository.IngredientRepository;
import com.abn.recipeservice.repository.RecipeRepository;
import com.abn.recipeservice.repository.RecipeSpecification;
import com.abn.recipeservice.service.RecipeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@Service
@RequiredArgsConstructor
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;
    private final IngredientRepository ingredientRepository;
    private final RecipeMapper recipeMapper;

    @Override
    public RecipeResponse save(RecipeRequest recipeRequest) {

        Recipe recipe = recipeMapper.recipeRequestToRecipe(recipeRequest);

        List<Long> ids = recipe
                .getIngredients()
                .stream()
                .map(ingredient -> ingredient.getId())
                .collect(Collectors.toList());

        List<Ingredient> ingredients = StreamSupport.stream(ingredientRepository.findAllById(ids).spliterator(), false)
                .collect(Collectors.toList());
        if (ingredients.size() != ids.size())
            throw new IngredientNotFoundException();

        recipe.setIngredients(ingredients);

        return recipeMapper.recipeToRecipeResponse(recipeRepository.save(recipe));
    }

    @Override
    public RecipeResponse get(Long recipeId) {
        return recipeMapper.recipeToRecipeResponse(recipeRepository.findById(recipeId).orElseThrow(RecipeNotFoundException::new));
    }

    @Override
    public void delete(Long recipeId) {
        recipeRepository.findById(recipeId).map(recipe -> {
            recipeRepository.deleteById(recipe.getId());
            return recipe;
        }).orElseThrow(RecipeNotFoundException::new);
    }

    @Override
    public RecipeResponse update(RecipeRequest recipeRequest, Long recipeId) {

        Recipe recipe = recipeRepository.findById(recipeId).map(rp -> recipeMapper.recipeRequestAndRecipeToRecipe(recipeRequest, rp)).orElseThrow(RecipeNotFoundException::new);
        List<Long> ids = recipe
                .getIngredients()
                .stream()
                .map(ingredient -> ingredient.getId())
                .collect(Collectors.toList());

        List<Ingredient> ingredients = StreamSupport.stream(ingredientRepository
                .findAllById(ids)
                .spliterator(), false)
                .collect(Collectors.toList());
        if (ingredients.size() != ids.size())
            throw new IngredientNotFoundException();

        recipe.setIngredients(ingredients);

        return recipeMapper.recipeToRecipeResponse(recipeRepository.save(recipe));
    }

    @Override
    public List<RecipeResponse> search(SearchRequest searchCriteriaRequest) {

        RecipeSpecification specification = new RecipeSpecification();

        recipeMapper.searchRequestToSearchCriteriaList(searchCriteriaRequest).forEach(searchCriteria -> {
            specification.add(searchCriteria);
        });

        return recipeMapper.recipeListToRecipeResponseList(recipeRepository.findAll(specification));
    }


}
