package com.abn.recipeservice.service.impl;

import com.abn.recipeservice.domain.Ingredient;
import com.abn.recipeservice.dto.IngredientRequest;
import com.abn.recipeservice.dto.IngredientResponse;
import com.abn.recipeservice.exception.IngredientNotFoundException;
import com.abn.recipeservice.mapper.IngredientMapper;
import com.abn.recipeservice.repository.IngredientRepository;
import com.abn.recipeservice.service.IngredientService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class IngredientServiceImpl implements IngredientService {

    private final IngredientRepository ingredientRepository;
    private final IngredientMapper ingredientMapper;

    @Override
    public IngredientResponse save(IngredientRequest ingredientRequest) {
        Ingredient ingredient = ingredientMapper.ingredientRequestToIngredient(ingredientRequest);
        return ingredientMapper.ingredientToIngredientResponse(ingredientRepository.save(ingredient));
    }

    @Override
    public IngredientResponse get(Long ingredientId) {
        return ingredientMapper.ingredientToIngredientResponse(ingredientRepository.save(ingredientRepository.findById(ingredientId).orElseThrow(IngredientNotFoundException::new)));
    }

    @Override
    public void delete(Long ingredientId) {
        ingredientRepository.findById(ingredientId).map(ingredient -> {
            ingredientRepository.deleteById(ingredient.getId());
            return ingredient;
        }).orElseThrow(IngredientNotFoundException::new);
    }

    @Override
    public IngredientResponse update(IngredientRequest ingredientRequest, Long ingredientId) {
        Ingredient ingredient = ingredientRepository.findById(ingredientId).orElseThrow(IngredientNotFoundException::new);
        ingredient = ingredientMapper.ingredientRequestAndIngredientToIngredient(ingredientRequest, ingredient);
        return ingredientMapper.ingredientToIngredientResponse(ingredientRepository.save(ingredient));
    }

    @Override
    public List<IngredientResponse> getAll(Integer page, Integer pageSize) {
        return ingredientMapper.recipeListToRecipeResponseList(ingredientRepository.findAll(PageRequest.of(page, pageSize)).getContent());
    }
}
