package com.abn.recipeservice.service;

import com.abn.recipeservice.dto.IngredientRequest;
import com.abn.recipeservice.dto.IngredientResponse;

import java.util.List;


public interface IngredientService {
    IngredientResponse save(IngredientRequest ingredientRequest);

    IngredientResponse get(Long ingredientId);

    void delete(Long ingredientId);

    IngredientResponse update(IngredientRequest ingredientRequest, Long ingredientId);

    List<IngredientResponse> getAll(Integer page, Integer pageSize);
}
