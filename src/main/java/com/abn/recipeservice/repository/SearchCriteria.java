package com.abn.recipeservice.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SearchCriteria {
    private String key;
    private String value;
    private QueryOperator operation;
    private DomainType domainType;
}
