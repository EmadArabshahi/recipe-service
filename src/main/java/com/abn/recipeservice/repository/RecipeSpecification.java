package com.abn.recipeservice.repository;

import com.abn.recipeservice.domain.Ingredient;
import com.abn.recipeservice.domain.Recipe;
import com.abn.recipeservice.domain.RecipeType;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class RecipeSpecification implements Specification<Recipe> {

    private List<SearchCriteria> list;

    public RecipeSpecification() {
        this.list = new ArrayList<>();
    }

    public void add(SearchCriteria criteria) {
        list.add(criteria);
    }


    @Override
    public Predicate toPredicate(Root<Recipe> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        List<Predicate> predicates = new ArrayList<>();

        Join<Recipe, Ingredient> recipeIngredientJoin = root.join("ingredients");

        for (SearchCriteria criteria : list) {

            Boolean domain = criteria.getDomainType().equals(DomainType.INGREDIENT);

            Optional.ofNullable(criteria.getOperation()).ifPresent(queryOperator -> {
                if (queryOperator.equals(QueryOperator.GREATER_THAN)) {
                    if (domain) {
                        predicates.add(builder.greaterThan(recipeIngredientJoin.get(criteria.getKey()), criteria.getValue()));
                    } else {
                        predicates.add(builder.greaterThan(
                                root.get(criteria.getKey()), criteria.getValue()));
                    }

                } else if (queryOperator.equals(QueryOperator.LESS_THAN)) {
                    if (domain) {
                        predicates.add(builder.lessThan(recipeIngredientJoin.get(criteria.getKey()), criteria.getValue()));
                    } else {
                        predicates.add(builder.lessThan(
                                root.get(criteria.getKey()), criteria.getValue()));
                    }
                } else if (queryOperator.equals(QueryOperator.GREATER_THAN_EQUAL)) {
                    if (domain) {
                        predicates.add(builder.greaterThanOrEqualTo(recipeIngredientJoin.get(criteria.getKey()), criteria.getValue()));
                    } else {
                        predicates.add(builder.greaterThanOrEqualTo(
                                root.get(criteria.getKey()), criteria.getValue()));
                    }
                } else if (queryOperator.equals(QueryOperator.LESS_THAN_EQUAL)) {
                    if (domain) {
                        predicates.add(builder.lessThanOrEqualTo(recipeIngredientJoin.get(criteria.getKey()), criteria.getValue()));
                    } else {
                        predicates.add(builder.lessThanOrEqualTo(
                                root.get(criteria.getKey()), criteria.getValue()));
                    }
                } else if (queryOperator.equals(QueryOperator.NOT_EQUAL)) {
                    if (domain) {
                        predicates.add(builder.notEqual(recipeIngredientJoin.get(criteria.getKey()), criteria.getValue()));
                    } else {
                        predicates.add(builder.notEqual(
                                root.get(criteria.getKey()), criteria.getValue()));
                    }
                } else if (queryOperator.equals(QueryOperator.EQUAL)) {
                    if (domain) {
                        predicates.add(builder.equal(recipeIngredientJoin.get(criteria.getKey()), criteria.getValue()));
                    } else {

                        predicates.add(builder.equal(
                                root.get(criteria.getKey()), getEnumOrString(criteria.getValue())));
                    }
                } else if (queryOperator.equals(QueryOperator.MATCH)) {
                    if (domain) {
                        predicates.add(builder.like(recipeIngredientJoin.get(criteria.getKey()), "%" + criteria.getValue().toLowerCase() + "%"));
                    } else {
                        predicates.add(builder.like(
                                builder.lower(root.get(criteria.getKey())),
                                "%" + criteria.getValue().toLowerCase() + "%"));
                    }
                } else if (queryOperator.equals(QueryOperator.MATCH_END)) {
                    if (domain) {
                        predicates.add(builder.like(recipeIngredientJoin.get(criteria.getKey()), criteria.getValue().toLowerCase() + "%"));
                    } else {
                        predicates.add(builder.like(
                                builder.lower(root.get(criteria.getKey())),
                                "%" + criteria.getValue().toLowerCase()));
                    }
                } else if (queryOperator.equals(QueryOperator.MATCH_START)) {
                    if (domain) {
                        predicates.add(builder.like(recipeIngredientJoin.get(criteria.getKey()), "%" + criteria.getValue().toLowerCase()));
                    } else {
                        predicates.add(builder.like(
                                builder.lower(root.get(criteria.getKey())),
                                "%" + criteria.getValue().toLowerCase()));
                    }
                } else if (queryOperator.equals(QueryOperator.IN)) {
                    if (domain) {
                        predicates.add(builder.in(recipeIngredientJoin.get(criteria.getKey())).value(criteria.getValue()));
                    } else {
                        predicates.add(builder.in(root.get(criteria.getKey())).value(criteria.getValue()));
                    }
                } else if (queryOperator.equals(QueryOperator.NOT_IN)) {
                    if (domain) {
                        predicates.add(builder.not(recipeIngredientJoin.get(criteria.getKey())).in(criteria.getValue()));
                    } else {
                        predicates.add(builder.not(root.get(criteria.getKey())).in(criteria.getValue()));
                    }
                }
            });
        }

        return builder.and(predicates.toArray(new Predicate[0]));
    }

    private Object getEnumOrString(String input) {

        return Arrays.stream(RecipeType.values())
                .filter(recipeType1 -> recipeType1.name().equals(input))
                .findFirst()
                .isPresent() ? RecipeType.valueOf(input) : input;
    }
}