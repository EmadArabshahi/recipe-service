package com.abn.recipeservice.repository;

import com.abn.recipeservice.domain.Ingredient;
import com.abn.recipeservice.domain.Recipe;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IngredientRepository extends PagingAndSortingRepository<Ingredient, Long> {
    Optional<Recipe> findByName(String name);
}
