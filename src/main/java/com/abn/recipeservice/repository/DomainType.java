package com.abn.recipeservice.repository;

public enum DomainType {
    RECIPE, INGREDIENT
}
