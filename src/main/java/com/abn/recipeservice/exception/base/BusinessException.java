package com.abn.recipeservice.exception.base;

import java.util.List;

public class BusinessException extends RuntimeException {

    protected List<String> messages;

    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
