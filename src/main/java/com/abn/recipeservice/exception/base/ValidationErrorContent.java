package com.abn.recipeservice.exception.base;

import java.util.List;

public class ValidationErrorContent extends ErrorContent {

    private final static String VALIDATION_ERROR_CODE = "1";
    private final static String VALIDATION_ERROR_MESSAGE = "Validation failure!";
    private final List<FieldErrorContent> errors;

    public ValidationErrorContent(List<FieldErrorContent> errors) {
        super(VALIDATION_ERROR_CODE, VALIDATION_ERROR_MESSAGE);
        this.errors = errors;
    }

    public List<FieldErrorContent> getErrors() {
        return errors;
    }
}
