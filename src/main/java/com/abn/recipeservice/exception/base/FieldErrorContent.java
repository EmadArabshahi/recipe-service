package com.abn.recipeservice.exception.base;

public class FieldErrorContent {
    private String code;
    private String field;
    private String message;

    public FieldErrorContent(String code, String message, String field) {
        this.code = code;
        this.field = field;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }

}
