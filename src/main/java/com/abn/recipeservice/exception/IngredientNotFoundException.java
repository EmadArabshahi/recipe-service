package com.abn.recipeservice.exception;

import com.abn.recipeservice.exception.base.NotFoundException;

public class IngredientNotFoundException extends NotFoundException {
}
