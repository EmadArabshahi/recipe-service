# Getting Started

### References

References:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.7.5/gradle-plugin/reference/html/)
* [Spring Test](https://docs.spring.io/spring-boot/docs/2.7.5/reference/htmlsingle/#features.testing)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.5/reference/htmlsingle/#web)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.7.5/reference/htmlsingle/#data.sql.jpa-and-spring-data)

### Usage

# Tech matters

Spring framework is used because it is very stable and has a solution for almost every problem. In this case, we have
several layers, namely controller, service, mapper between service and controller, and repository layer. which has been
used by programmers for a long time and has also considered facilities for the Spring framework. JPA specification is
used for the search part so that we can have a dynamic search. For the search endpoint, the post method is used because
the body of the request is a bit complicated and this happened only for better readability, otherwise I am aware of the
fact that post is not an idempotent method. Also, in this project, MapStruct is used, which is responsible for the
mapper layer. This library has many features and I have done many projects with it. Swagger has been used to document
REST APIs

## Run

cd {root} ./gradlew build cd ./build/libs/ java -jar recipe-service-0.0.1.jar

## Search

This is a class that is stored in the database and includes these fields

    private Long id;  
	private RecipeType recipeType;  
	private String name;  
	private String instruction;  
	private Integer numberServings;  
	private List<Ingredient> ingredients;  
	private Date createDate;  
	private Date updateDate;

As you can see, it consists of these fields. If you want to have a dynamic search, you can use the following class

    public class SearchRequest {  
    private List<CriteriaRequest> searchCriteria;  
	}

    public class CriteriaRequest {  
    private String key;  
    private String value;  
    private QueryOperator operation;  
    private DomainType domainType;  
	}

    public enum QueryOperator {  
    GREATER_THAN,  
	LESS_THAN,  
	......... } 

    public enum DomainType {  
	    RECIPE,INGREDIENT  
	}

You can give API a list of actions you want to perform, on all fields or on some of them. There is no limit. For this,
give the name of the field to the key and its value to the value. And give the type of that operation to QueryOperator.
Also, determine on which entity you want it to be done and mark it with an DomainType. You can have list of
CriteriaRequest in a time, send SearchRequest to server and have a result which is list of Recipes
